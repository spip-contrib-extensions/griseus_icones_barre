<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// G
	'griseusicobarre_nom' => 'Griseus : icônes barre d\'édition',
	'griseusicobarre_slogan' => 'Des icônes pour la barre d\'édition du porte-plume',
	'griseusicobarre_description' => 'Ce plugin fait partie du thème "Griseus". Il remplace les icônes de la barre d\'édition du porte-plume par des icônes inspirées du jeu d\'icônes "Faïence" de Matthieu James.'
);

?>